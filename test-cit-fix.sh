#!/bin/bash

source vars.sh

./init-svn.sh

echo "========= faking a fix in $CIT ============"

svn sw $CIT $PROJECTDIR $SVNARGS $ASDEV
./fix.sh

./sprint-release.sh CIT WB

svn sw $trunk $PROJECTDIR $SVNARGS $ASDEV
svn up $PROJECTDIR $SVNARGS $ASDEV

FILECOUNT=`find $PROJECTDIR/src/test/resources/ -type f -name *.txt | wc -l`
echo "found $FILECOUNT *.txt files in src/test/resources"
if [ 1 != $FILECOUNT ]
then
	>&2 echo "FAIL: expected 1 file but found $FILECOUNT"
	exit 1
else
  echo "Everything ok"
fi
