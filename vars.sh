#!/bin/bash

function maven_version () {
	 mvn -q -DforceStdout help:evaluate -Dexpression=project.version 2> /dev/null
}

if [ ! -z ${SVN_PROJECT+x} ] ; then SVN_PROJECT="/${SVN_PROJECT}"; fi

SVNPORT="7443"
SVNROOT="http://localhost:${SVNPORT}/svn"

echo "$SVN_PROJECT"
trunk="^$SVN_PROJECT/trunk"
CIT="^$SVN_PROJECT/branches/CIT"
WB="^$SVN_PROJECT/branches/WB"

PROJECTDIR="svn-workflow"

JENKINS="jenkins"
JENKINSPW="jenkins"

DEV="developer"
DEVPW="developer"

ASJENKINS="--username $JENKINS --password $JENKINSPW --non-interactive --no-auth-cache"
ASDEV="--username $DEV --password $DEVPW --non-interactive --no-auth-cache"

SVNARGS="-q "

DOCKERWAIT=10
