# svn-workflow

Inspired by the [git-workflow](https://nvie.com/posts/a-successful-git-branching-model/) this project aims to help people still using SVN
to use a similar approach of branching and merging. (And yes, i'm completely aware of the irony to use git for versioning this :-)

It consists of a branch for production ready code (called "WB" short for the german word "Wirkbetrieb"), another one for final acceptance tests or complex interation test (called CIT),
and the main development branch called "trunk".

As branching and merging is more difficult with SVN than with git we use fixed points in time where the branches are integrated. This would be a sprint end for example, where
finished features are integrated into the trunk and pushed upwards for integration testing. Before that, the current state of the CIT branch may be pushed to production. But it may also reside
in CIT and be merged only with the changes from trunk and be merged into WB later. 

Between releases changes can be done on the production or integration branches and must be merged down before a release in order to prevent merge conflicts. Especially for difficult 
or large changes such down merges should be done manually and conflicts should be resolved. But simple ones can be merged automatically before changes are merged upwards. 

New features are implemented in feature branches taken from trunk. This is especially usefull if a feature is taking longer than the usual sprint length, completion of other needed 
features are deprioritized or any other reason the feature is not ready to go into production at the end of the sprint. It is merged into trunk only when it is ready and everything is in place
for it. 

```  
                      o-----------o   |                                          |
                     / fix in prod \  |                                          |
WB              o---o---------------o-|------------x------------o----------------|-------	
               /                      | pull down   \           ^                |
              /                       | merge to CIT \         / push CIT        |  
             /                        | branch        \       /  to WB           |
            /                         |                \     /                   |
CIT        o-o--------------o---------|-----------------v-x-x--------o-----------|--------
          /   \ fix in cit /          |         pull down  \         ^           |
         /     o----------o           |         merge to    \       / push trunk |
        /      o----------------o     |         trink        \     /  to CIT     |
       /      /   feature 1      \    |                       \   /              |
trunk o--o---o--------------------o---|------------------------v-x---------------|--------
          \ long running feature      |                                          |
           o--------------------------|------------------------------------------|--------
                                      |                                          |
                                      |<-----------Sprint Release--------------->|
```   

The whole part named "Sprint Release" must be done automatically and therefore I've written the bash script `sprint-release.sh`

## Versioning 

But there is still an issue with this when using a build tool, that needs a file to contain a version number of the build artifact and that file is tracked by the version control system itself. One example is the `pom.xml` for maven. 

### Preventing merge conflicts

See the "fix in prod" for example. If there's an issue in production that you need to fix, you'll fix it in a branch and merge it back. Your changes will be tested and need to be released to production immediatly. So you need to assign a new version number for your deliverable artifact. 

In the projects I use to work for, this is done with the maven release plugin that is executed on the build server. During its run it will assign a fixed version number (remove the "-SNAPSHOT") and tag that (=create a tag branch named after the assigned version). Then it will increase the version in the WB branch to the next SNAPSHOT and commit that change. We will actually have two commits on the WB branch before the next sprint release, one of which changed the version in the pom. 

The same will happen on the CIT branch when a fix is commited there and a new delivery will be done from the CIT branch before the next sprint release. This will definitely lead to a conflict during the pull down phases. 

To make things worse the trunk version must be increased after every sprint (if the the codebase was actually really changed during it). This is necessary to have distinct versions in each branch 
after the push up (see below). 

To circumvent those conflicts the script `sprint-release-sh` is doing a little trick that needs the following assumption to always be true: Versions are only changed by a specific svn user.

This allows us to "block" revisions that are made by this user when a branch is merged into another. To achieve this, the `merge` command of svn provides the option `--record-only` which effectivly adds the revision numbers passed to the merge request to the `svn:mergeinfo` property. Future merges between the branches will no longer try to merge those revisions and prevent conflicts on the `pom.xml`s.

### Version shifting

As said before, the code base in each branch must have a distinctive version which that can be increased. So if a fix is made on the production branch we need to be able to create a new final version. At the same time a fix must be doable in the integration test branch and this version must be different from the other branches too. 
This can only be achieved if, at any given point in time, the versions between the branches differ in at least the minor version.

To realize this we increase the minor version in the trunk at the end of each sprint release. So in the next release this version is pushed up to integration test (CIT). And in the release after that, the version from CIT is pushed to the production (WB), while the new next trunk version is moved to the CIT.

```   
      x.y.*-SNAPSHOT        x.y+1.*-SNAPSHOT      x.y+2.*-SNAPSHOT
WB    --------------------^--------------------^----------------------
                         /                    /
      x.y+1.*-SNAPSHOT  /   x.y+2.*-SNAPSHOT /    x.y+3.*-SNAPSHOT
CIT   -----------------x--^-----------------x-- ^----------------------
                         /                     /
      x.y+2.*-SNAPSHOT  /   x.y+3.0-SNAPSHOT  /   x.y+4.0-SNAPSHOT
trunk -----------------x---------------------x----------------------
                       ^                     ^
                       |                     |
                 sprint release        sprint release
```  


