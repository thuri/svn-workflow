#!/bin/bash 

source vars.sh

echo "========== Starting SVN Server and import project ==========="
rm -Rf svn-workflow
sudo docker stop svn-server
sudo docker run --rm -d -p${SVNPORT}:80 --name svn-server svn-server:1 
echo "waiting ${DOCKERWAIT}s for docker container to start"
sleep $DOCKERWAIT 
svn import example-project "$SVNROOT/svn-workflow/trunk" $SVNARGS $ASDEV -m "initial import"
svn co "$SVNROOT/svn-workflow/trunk" $PROJECTDIR $SVNARGS $ASDEV

echo ""
echo "=========== Initializing the project structure =============="

# this will be the final branch structure
#
#  WB              °---------------- 0.0.1-SNAPSHOT
#                 /
#                /
#               /
#  CIT         °---------------- 0.1.0-SNAPSHOT
#             /
#            /
#           /
#  trunk --°---------------- 0.2.0-SNAPSHOT

cd $PROJECTDIR

echo "push the trunk to the new CIT branch. effectivly pushing the current version (0.0.1-SNAPSHOT) to the CIT"
svn copy "$trunk" "$CIT" -m "create CIT branch" --parents $SVNARGS $ASDEV
echo "update the version in the trunk to 0.2.0-SNAPSHOT"
mvn versions:set -q -DnewVersion=0.2.0-SNAPSHOT -DgenerateBackupPoms=false 2>/dev/null
svn commit -m "init trunk version as 2 minors ahead of WB" $SVNARGS $ASJENKINS

echo "copy the current CIT to the WB branch => 0.0.1-SNAPSHOT is now the version in the WB branch"
svn copy "$CIT" "$WB" -m "create WB branch" --parents $SVNARGS $ASDEV
echo "switch to the CIT branch so we can change the version there to 0.1.0-SNAPSHOT"
svn sw "$CIT" $SVNARGS $ASDEV
mvn versions:set -q -DnewVersion=0.1.0-SNAPSHOT -DgenerateBackupPoms=false 2>/dev/null
svn commit -m "init cit version as 1 minor ahead of WB" $SVNARGS $ASJENKINS

cd ..
