FROM elleflorio/svn-server

RUN htpasswd -b /etc/subversion/passwd jenkins jenkins && \
    htpasswd -b /etc/subversion/passwd developer developer && \
    svnadmin create /home/svn/svn-workflow && \
	  chown -R apache:root /home/svn/svn-workflow 
COPY subversion-access-control /etc/subversion/
