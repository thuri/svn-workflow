#!/bin/bash

source vars.sh

cd $PROJECTDIR

filename=src/test/resources/$RANDOM.txt
echo "Hello World" > $filename
svn add $filename $SVNARGS $ASDEV
svn commit -m "added $filename" $SVNARGS $ASDEV
echo "added file $filename"

mvn --batch-mode -q release:update-versions 2> /dev/null
ver=$(maven_version)
echo "new version $ver"
svn commit -m "updated version to $ver" $SVNARGS $ASJENKINS
svn up $SVNARGS $ASDEV

cd ..
