#!/bin/bash

function block_jenkins_revs() {
	blockedRevs=""
	for sREV in $(svn mergeinfo "$1" --show-revs=eligible $ASDEV ) 
	do
		rev=$(echo "$sREV" | cut -d'r' -f2) # remote the "r" at the beginning of the output 
		authoredByJenkins=
		if [ ! -z $(svn log "$1" --xml -c$rev $SVNARGS $ASDEV | grep "<author>${JENKINS}</author>") ]
		then 
	 		blockedRevs="${blockedRevs}-c$rev "
		fi
	done
	if [ ! -z "${blockedRevs}" ] 
	then 
		echo "  blocking revs $blockedRevs"
		svn merge "$1" $blockedRevs --record-only $SVNARGS $ASDEV 
		svn commit -m "blocked $blockedRevs from $1" $SVNARGS $ASDEV
		svn up $SVNARGS $ASDEV
		echo "  revs $blockedRevs blocked"
	fi
}

function block_and_merge() {
	echo "Switching to $2 ..." 
	svn sw "$2" $SVNARGS $ASDEV
	echo "  version in $2: $(maven_version)"
	echo "  merging $1 to $2"
	block_jenkins_revs "$1"
	svn merge "$1" $SVNARGS $ASDEV
	svn commit -m "merged from $1" $SVNARGS $ASDEV
	echo "  version in $2: $(maven_version)"
}

function up_merge() {
	echo "Merging $1 to $2"
	svn sw "$2" $SVNARGS $ASDEV
	echo "  version before: $(maven_version)"
	svn  merge "$1" $SVNARGS  $ASDEV
	echo "  version after: $(maven_version)"
	svn commit -m "merged from $1" $SVNARGS $ASDEV
}

function increment_minor() {
	VERSION=$(mvn -q -DforceStdout help:evaluate -Dexpression=project.version 2> /dev/null)
	pat='^([0-9]+)(\.([0-9]+)(\.([0-9]+))?)?(-SNAPSHOT)?'
	[[ $VERSION =~ $pat ]]

	major=${BASH_REMATCH[1]}
	minor=${BASH_REMATCH[3]}
	snapshot=${BASH_REMATCH[6]}

	((minor = minor + 1))

	VERS="" 
	for v in $major $minor 0
	do
		if [ ! -z "${v}x" ] ; then VERS="${VERS}.$v"; fi
	done
	echo "${VERS:1}${snapshot}"
}

source vars.sh

cd $PROJECTDIR

echo "=================== pull down ====================="

block_and_merge "$WB" "$CIT"
block_and_merge "$CIT" "$trunk"

echo "=================== push up ======================="

for var in "$@"
do 
	if [ "$var" == "WB" ];  then doCIT="1"; doWB="1";  fi
	if [ "$var" == "CIT" ]; then doCIT="1"; fi
done

if [ "$doWB" == "1" ];  then up_merge "$CIT" "$WB"; fi
if [ "$doCIT" == "1" ]; then up_merge "$trunk" "$CIT"; fi

echo "Update trunk version"
svn sw "$trunk" $SVNARGS $ASJENKINS
echo "  version before: $(maven_version)"
mvn versions:set -q -DnewVersion=$(increment_minor) -DgenerateBackupPoms=false 2> /dev/null
svn commit -m "updated version" $SVNARGS $ASJENKINS
echo "  version after: $(maven_version)"

# An additional down merge is necessary to synch the merge info in the downstream
# after the upstream has changed during the up merge
# otherwise the next down merge will fail if a fix is made on CIT or WB or trunk after this run
# this is because trunk for example "inherits" the mergeinfo from CIT about WB.
# which would lead to situations where a revision is tried to be merged into trunk because 
# trunk mergeinfo to WB says so. Irrelevant if the WB->CIT merge before was OK.
echo "=================== pull down again =============="
block_and_merge "$WB" "$CIT"
block_and_merge "$CIT" "$trunk"

cd ..
