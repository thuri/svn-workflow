#!/bin/bash

source vars.sh

./init-svn.sh

echo "========= faking a fix in $WB  ============"

svn sw $WB $PROJECTDIR $SVNARGS $ASDEV
./fix.sh

echo "========= faking new feature in $trunk  ============"

svn sw $trunk $PROJECTDIR $SVNARGS $ASDEV
./fix.sh

echo "========= release sprint ================="
./sprint-release.sh CIT

echo "========= faking new feature in $trunk  ============"

svn sw $trunk $PROJECTDIR $SVNARGS $ASDEV
./fix.sh

echo "========= release sprint ================="
./sprint-release.sh CIT

echo "========= faking a fix in $CIT  ============"

svn sw $CIT $PROJECTDIR $SVNARGS $ASDEV
./fix.sh

echo "========= faking new feature in $trunk  ============"

svn sw $trunk $PROJECTDIR $SVNARGS $ASDEV
./fix.sh

echo "========= release sprint ================="
./sprint-release.sh CIT WB

svn sw $trunk $PROJECTDIR $SVNARGS $ASDEV
svn up $PROJECTDIR $SVNARGS $ASDEV

FILECOUNT=`find $PROJECTDIR/src/test/resources/ -type f -name *.txt | wc -l`
echo "found $FILECOUNT *.txt files in src/test/resources"
if [ 5 != $FILECOUNT ]
then
	>&2 echo "FAIL: expected 5 file but found $FILECOUNT"
	exit 1
else
  echo "Everything ok"
fi

svn sw $CIT $PROJECTDIR $SVNARGS $ASDEV
svn up $PROJECTDIR $SVNARGS $ASDEV

FILECOUNT=`find $PROJECTDIR/src/test/resources/ -type f -name *.txt | wc -l`
echo "found $FILECOUNT *.txt files in src/test/resources"
if [ 5 != $FILECOUNT ]
then
	>&2 echo "FAIL: expected 5 file but found $FILECOUNT"
	exit 1
else
  echo "Everything ok"
fi

svn sw $WB $PROJECTDIR $SVNARGS $ASDEV
svn up $PROJECTDIR $SVNARGS $ASDEV

FILECOUNT=`find $PROJECTDIR/src/test/resources/ -type f -name *.txt | wc -l`
echo "found $FILECOUNT *.txt files in src/test/resources"
if [ 4 != $FILECOUNT ]
then
	>&2 echo "FAIL: expected 4 file but found $FILECOUNT"
	exit 1
else
  echo "Everything ok"
fi
